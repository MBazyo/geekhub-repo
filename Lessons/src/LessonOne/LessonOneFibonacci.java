package LessonOne;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class LessonOneFibonacci {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите количество чисел последовательности Фибоначчи:");

        try{
            int n = Integer.parseInt(br.readLine());

            int a = 1, b = 1;
            System.out.print(a + " " + b);
            int fib = 2, i = 2;
            while (i < n) {
                fib = a + b;
                a = b;
                b = fib;
                System.out.print(" " + fib);
                i++;
            }

        }catch(NumberFormatException nfe){
            System.err.println("Ошибка: Вы ввелы не целое число или текст");
        }

    }

}
