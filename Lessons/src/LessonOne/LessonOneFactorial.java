package LessonOne;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class LessonOneFactorial {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите число для которого вычесляется факториал:");

        try{
            int n = Integer.parseInt(br.readLine());
            System.out.println("Факториал числа - "+factorial(n));

        }catch(NumberFormatException nfe){
            System.err.println("Ошибка: Вы ввелы не целое число или текст");
        }

    }

    public static int factorial(int n)
    {
        int rez = 1;
        for (int i = 1; i <= n; ++i) rez = rez* i;
        return rez;
    }

}
