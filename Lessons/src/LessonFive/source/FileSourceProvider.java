package LessonFive.source;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Implementation for loading content from local file system.
 * This implementation supports absolute paths to local file system without specifying file:// protocol.
 * Examples c:/1.txt or d:/pathToFile/file.txt
 */
public class FileSourceProvider implements SourceProvider {

    @Override
    public boolean isAllowed(String pathToSource) { // можно ли загрузить ресурс по указаному пути
        File file = new File(pathToSource);
        return file.canRead();
    }

    @Override
    public String load(String pathToSource) throws IOException {   // читает с файла
        StringBuffer text = new StringBuffer();
        File file = new File(pathToSource);
        try(BufferedReader buffered = new BufferedReader(new FileReader(file))){
            String fileLine;
            while ((fileLine = buffered.readLine()) != null){
                text.append(fileLine);
                text.append("\n");
            }

        }catch (IOException o){
            System.out.println("Ошибка загрузки файла (FileSourseProvider - load)");
            throw new IOException();
        }

        return text.toString();
    }
}

