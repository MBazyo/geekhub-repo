package LessonFive.source;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Implementation for loading content from specified URL.<br/>
 * Valid paths to load are http://someurl.com, https://secureurl.com, ftp://frpurl.com etc.
 */
public class URLSourceProvider implements SourceProvider {

    @Override
    public boolean isAllowed(String pathToSource) { // можно ли загрузить ресурс по указаному пути
        try{
            new URL(pathToSource);
            return true;

        } catch (MalformedURLException e) {
            return false;
        }

    }

    @Override
    public String load(String pathToSource) throws IOException {  //считывает данные с файла
        StringBuffer file = new StringBuffer();
        URLConnection url = new URL(pathToSource).openConnection();

        try(BufferedReader buffered = new BufferedReader(new InputStreamReader(url.getInputStream()))){
            String fileLine;
            while ((fileLine = buffered.readLine()) != null){
                file.append(fileLine);
                file.append("\n");
            }

        }catch (IOException o){
            System.out.println("Ошибка загрузки файла (URLSourseProvider - load)");
            throw new IOException();
        }

        return file.toString();

    }
}
