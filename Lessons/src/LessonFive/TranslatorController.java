package LessonFive;

import LessonFive.source.SourceLoader;
import LessonFive.source.URLSourceProvider;

import java.io.IOException;
import java.util.Scanner;

public class TranslatorController {

    public static void main(String[] args) throws IOException {
        //initialization
        SourceLoader sourceLoader = new SourceLoader();
        Translator translator = new Translator(new URLSourceProvider());

        Scanner scanner = new Scanner(System.in);
        String command = scanner.next();
        while (!"exit".equals(command)) {
            try {
                String source = sourceLoader.loadSource(command);
                String translation = translator.translate(source);

                System.out.println("Результат\n Оригинал: \n" + source);
                System.out.println(" Перевод: \n" + translation);
            } catch (IOException e) {

            }
            command = inputPath(scanner);
        }
    }

    private static String inputPath(Scanner scanner) {
        String command;
        System.out.print("\nПропишите путь к файлу или 'exit' чтобы выйти\n" +
                "(Пример: 'https://dl.dropboxusercontent.com/u/14434019/en.txt' или 'D:/1.txt'): ");
        command = scanner.next();
        return command;
    }
}
