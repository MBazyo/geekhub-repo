package LessonSeven.storage;

import LessonSeven.objects.Entity;
import LessonSeven.objects.Ignore;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Implementation of {@link org.geekhub.storage.Storage} that uses database as a storage for objects.
 * It uses simple object type names to define target table to save the object.
 * It uses reflection to access objects fields and retrieve data to map to database tables.
 * As an identifier it uses field id of {@link org.geekhub.objects.Entity} class.
 * Could be created only with {@link java.sql.Connection} specified.
 */
public class DatabaseStorage implements Storage {
    private Connection connection;

    public DatabaseStorage(Connection connection) {
        this.connection = connection;
    }

    @Override
    public <T extends Entity> T get(Class<T> clazz, Integer id) throws Exception {
        //this method is fully implemented, no need to do anything, it's just an example
        String sql = "SELECT * FROM " + clazz.getSimpleName() + " WHERE id = " + id;
        try (Statement statement = connection.createStatement()) {
            List<T> result = extractResult(clazz, statement.executeQuery(sql));
            return result.isEmpty() ? null : result.get(0);
        }
    }

    @Override
    public <T extends Entity> List<T> list(Class<T> clazz) throws Exception {
        String sql = "SELECT * FROM " + clazz.getSimpleName();
        try (Statement statement = connection.createStatement()) {
            return extractResult(clazz, statement.executeQuery(sql));
        }
    }

    @Override
    public <T extends Entity> boolean delete(T entity) throws Exception {
        String sql = "DELETE FROM " + entity.getClass().getSimpleName() + " WHERE id = " + entity.getId();
        try (Statement statement = connection.createStatement()) {
            return statement.execute(sql);
        }
    }

    @Override
    public <T extends Entity> void save(T entity) throws Exception {
        Map<String, Object> data = prepareEntity(entity);

        String sql = null;
        StringBuilder query = new StringBuilder();

        if (entity.isNew()) {
            StringBuilder values = new StringBuilder(" VALUES (");
            String[] properties = new String[data.size()];
            data.keySet().toArray(properties);

            query.append("INSERT INTO ");
            query.append(entity.getClass().getSimpleName());
            query.append(" (");

            for (int i = 0; i < properties.length; i++) {
                query.append(properties[i]);
                if (data.get(properties[i]) instanceof String) {
                    values.append("'");
                    values.append(data.get(properties[i]));
                    values.append("'");
                } else {
                    values.append(data.get(properties[i]));
                }

                if (i < (properties.length - 1)) {
                    query.append(", ");
                    values.append(", ");
                }
            }
            values.append(")");
            query.append(")");
            query.append(values);
        } else {
            String[] properties = new String[data.size()];
            data.keySet().toArray(properties);

            query.append("UPDATE ");
            query.append(entity.getClass().getSimpleName());
            query.append(" SET ");

            for (int i = 0; i < properties.length; i++) {
                query.append(properties[i]);
                query.append(" = ");
                if (data.get(properties[i]) instanceof String) {
                    query.append("'");
                    query.append(data.get(properties[i]));
                    query.append("'");
                } else {
                    query.append(data.get(properties[i]));
                }

                if (i < (properties.length - 1)) {
                    query.append(", ");
                }
            }
            query.append(" WHERE id = ");
            query.append(entity.getId());
        }

        sql = query.toString();

        try (Statement statement = connection.createStatement()) {
            if (entity.isNew()) {
                statement.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
                ResultSet result = statement.getGeneratedKeys();
                result.first();
                entity.setId(result.getInt(1));
            } else {
                statement.executeUpdate(sql);
            }
        }

    }

    //converts object to map, could be helpful in save method
    private <T extends Entity> Map<String, Object> prepareEntity(T entity) throws Exception {
        Map<String, Object> data = new HashMap<>();
        Field[] fields = entity.getClass().getDeclaredFields();

        for (Field field : fields) {
            if (!field.isAnnotationPresent(Ignore.class)) {
                Method getter = new PropertyDescriptor(field.getName(), entity.getClass()).getReadMethod();
                data.put(field.getName(), getter.invoke(entity));
            }
        }

        return data;
    }


    //creates list of new instances of clazz by using data from resultset
    private <T extends Entity> List<T> extractResult(Class<T> clazz, ResultSet resultset) throws Exception {
        List<T> entities = new ArrayList<>();

        while (resultset.next()) {
            T entity = clazz.newInstance();
            Field[] fields = clazz.getDeclaredFields();

            for (Field field : fields) {
                if (!field.isAnnotationPresent(Ignore.class)) {
                    Object value = resultset.getObject(field.getName());
                    Method setter = new PropertyDescriptor(field.getName(), clazz).getWriteMethod();
                    setter.invoke(entity, value);
                }
            }

            entity.setId(resultset.getInt("id"));
            entities.add(entity);
        }

        return entities;
    }
}