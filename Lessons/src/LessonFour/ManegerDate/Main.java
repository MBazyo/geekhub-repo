package LessonFour.ManegerDate;

import java.util.*;
import java.util.Calendar;

/**
 * Created with IntelliJ IDEA.
 * User: User
 * Date: 04.12.13
 * Time: 14:14
 * To change this template use File | Settings | File Templates.
 */
public class Main {

    public static void main(String[]args){
        LessonFour.ManegerDate.Calendar map = new LessonFour.ManegerDate.Calendar();
        java.util.Calendar calendar = java.util.Calendar.getInstance();

        calendar.add(Calendar.DATE, -2);
        map.addTask(calendar.getTime(), new Task("яблуко", "Семеренка"));
        calendar.add(Calendar.DATE, -2);
        map.addTask(calendar.getTime(), new Task("яблуко", "Антоновка"));
        calendar.add(Calendar.HOUR, -2);
        map.addTask(calendar.getTime(), new Task("яблуко", "Айдаред"));
        calendar.add(Calendar.DATE, -2);
        map.addTask(calendar.getTime(), new Task("яблуко", "Бреберн"));
        calendar.add(Calendar.HOUR, -2);
        map.addTask(calendar.getTime(), new Task("яблуко", "Гала"));

        calendar.add(Calendar.DATE, -2);
        map.addTask(calendar.getTime(), new Task("ягода", "Малина"));
        calendar.add(Calendar.HOUR, -2);
        map.addTask(calendar.getTime(), new Task("ягода", "Смородина"));
        calendar.add(Calendar.DATE, -2);
        map.addTask(calendar.getTime(), new Task("ягода", "Ежевика"));

        calendar.add(Calendar.DATE, -2);
        map.addTask(calendar.getTime(), new Task("овощ", "Помидор"));
        calendar.add(Calendar.DATE, -2);
        map.addTask(calendar.getTime(), new Task("овощ", "Капуста"));
        calendar.add(Calendar.HOUR, -2);
        map.addTask(calendar.getTime(), new Task("овощ", "Огурец"));
        calendar.add(Calendar.HOUR, -2);
        map.addTask(calendar.getTime(), new Task("овощ", "Буряк"));
        calendar.add(Calendar.HOUR, -4);
        map.addTask(calendar.getTime(), new Task("овощ", "Буряк"));

        System.out.println("getTasksByCategories");
        for(Task task: map.getTasksByCategory("яблуко")){
            System.out.print(task.getDescription() + ", ");
        }

        System.out.println(" ");
        System.out.println("getTasksByCategories");

        for (Map.Entry<String, List<Task>> entry: map.getTasksByCategories().entrySet()){
            System.out.print(entry.getKey() + ": ");

            for (Task task: entry.getValue()){
                System.out.print(task.getDescription() + ", ");
            }
            System.out.println();
        }

        System.out.println(" ");
        System.out.println("getTasksForToday");
        for (Task task: map.getTasksForToday()){
            System.out.printf("%s\t-\t%s\n", task.getCategory(), task.getDescription());
        }


    }
}

