package LessonFour.ManegerDate;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: User
 * Date: 04.12.13
 * Time: 14:14
 * To change this template use File | Settings | File Templates.
 */
public interface TaskManager  {

    public void addTask(Date date, Task task);

    public void removeTask(Date date);

    public Collection<String> getCategories();

    //For next 3 methods tasks should be sorted by scheduled date
    public Map<String, List<Task>> getTasksByCategories();

    public List<Task> getTasksByCategory(String category);

    public List<Task> getTasksForToday();
}
