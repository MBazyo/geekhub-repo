package LessonFour.ManegerDate;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: User
 * Date: 04.12.13
 * Time: 14:22
 * To change this template use File | Settings | File Templates.
 */
public class Calendar implements TaskManager {
    TreeMap<Date,Task> map = new TreeMap<Date, Task>();

    @Override
    public void addTask(Date date, Task task) {
        map.put(date, task);
    }

    @Override
    public void removeTask(Date date) {
        map.remove(date);
    }

    @Override
    public Collection<String> getCategories() {
        Set<String> cat = new HashSet<String>();

        for(Task task : map.values()){
            cat.add(task.getCategory());
        }

        return cat;
    }

    @Override
    public Map<String, List<Task>> getTasksByCategories() {
        HashMap<String, List<Task>> getCategory = new HashMap<String, List<Task>>();

        for (String category : getCategories()){
            getCategory.put(category, getTasksByCategory(category));
        }

        return getCategory;
    }

    @Override
    public List<Task> getTasksByCategory(String category) {
        List<Task> list = new ArrayList<Task>();

        for(Task task : map.values()){
            if(task.getCategory().equals(category)){
                list.add(task);
            }

        }

        return list;
    }

    @Override
    public List<Task> getTasksForToday() {
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.set(java.util.Calendar.HOUR, 0);
        calendar.set(java.util.Calendar.MINUTE, 0);
        calendar.set(java.util.Calendar.SECOND, 0);

        Date today = calendar.getTime();
        calendar.add(java.util.Calendar.DATE, 1);
        Date tomorrow = calendar.getTime();

        return new ArrayList<Task>(map.subMap(today, tomorrow).values());

    }
}
