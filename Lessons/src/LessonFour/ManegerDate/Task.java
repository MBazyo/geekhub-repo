package LessonFour.ManegerDate;

/**
 * Created with IntelliJ IDEA.
 * User: User
 * Date: 04.12.13
 * Time: 14:15
 * To change this template use File | Settings | File Templates.
 */
public class Task {
    private String category;
    private String description;

    public Task(String category, String description){
        this.category = category;
        this.description = description;
    }

    public String getCategory(){
        return category;
    }

    public String getDescription(){
        return description;
    }

}
