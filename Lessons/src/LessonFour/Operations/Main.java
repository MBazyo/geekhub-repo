package LessonFour.Operations;

import java.util.HashSet;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: User
 * Date: 05.11.13
 * Time: 22:46
 * To change this template use File | Settings | File Templates.
 */
public class Main {

    public  static void main(String[]args){
        Set<String> a = new HashSet<String>();
        Set<String> b = new HashSet<String>();
        Set<String> c = new HashSet<String>();
        OperationSet operation  = new OperationSet();

        a.add("time");
        a.add("is");
        a.add("money");

        b.add("money");
        b.add("is");
        b.add("time");

        c.add("life");
        c.add("freedome");
        c.add("doll");
        c.add("kithen");

        System.out.println("Same set String");

        if(operation.equals(a,b)){
            System.out.println("They == "+a);
        }else{
            System.out.println("They != ");
        }


        System.out.println("Union "+operation.union(a,b));
        System.out.println("Subtract "+operation.subtract(a,b));
        System.out.println("Intersect "+operation.intersect(a,b));
        System.out.println("Symmetric subtract "+operation.symmetricSubtract(a,b));

        System.out.println();
        System.out.println("Different set String");

        if (operation.equals(a,c)){
            System.out.println("They == "+a);
        }else{
            System.out.println("They != ");
        }

        System.out.println("Union "+operation.union(a,c));
        System.out.println("Subtract "+operation.subtract(a,c));
        System.out.println("Intersect "+operation.intersect(a,c));
        System.out.println("Symmetric subtract "+operation.symmetricSubtract(a,c));

    }
}
