package LessonFour.Operations;

import java.util.HashSet;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: User
 * Date: 05.11.13
 * Time: 22:51
 * To change this template use File | Settings | File Templates.
 */
public class OperationSet implements SetOperations{

    @Override
    public boolean equals(Set a, Set b) {   //Два множества А и В равны (А=В), если они состоят из одних и тех же элементов
        if (a.size() != b.size()) {
            return false;
        }
        if(!b.containsAll(a)){    // Returns true if this set contains the specified element.
                return false;
            }
        return true;
    }

    @Override //Объединением (суммой) множеств А и В называется множество А ∪ В,элементы которого принадлежат хотя бы одному из этих множеств
    public Set union(Set a, Set b) {
        Set c = new HashSet(a);
        c.addAll(b);    //Adds all of the elements in the specified collection to this set if they're not already present (optional operation).
        return c;
    }

    @Override
    public Set subtract(Set a, Set b) {   //Разностью множеств А и В называется множество АВ, элементы которого принадлежат множеству А, но не принадлежат множеству В
        Set c = new HashSet(a);
        c.removeAll(b);     // Removes from this set all of its elements that are contained in the specified collection (optional operation)
        return c;
    }

    @Override
    public Set intersect(Set a, Set b) {   //Пересечением (произведением) множеств А и В называется множество А ∩ В, элементы которого принадлежат как множеству А, так и множеству В
        Set c = new HashSet(a);
        c.retainAll(b);  //Retains only the elements in this set that are contained in the specified collection (optional operation)
        return c;
    }

    @Override
    public Set symmetricSubtract(Set a, Set b) {  //Симметричной разностью множеств А и В называется множество А Δ В, являющееся объединением разностей множеств АВ и ВА, то есть А Δ В = (АВ) ∪ (ВА)
        Set c = new HashSet(a);
        c = union(subtract(a,b),subtract(b,a));    // А Δ В = (АВ) ∪ (ВА)
        return c;
    }
}
