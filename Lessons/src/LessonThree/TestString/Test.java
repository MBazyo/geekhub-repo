package LessonThree.TestString;

public class Test {
    public static void main(String[] args) {
        testString();
        testStringBuilder();
        testStringBuffer();
    }

    private static void testString() {
        long startTime = 0, endTime;
        String s = new String("RadioRoc's");
        System.out.println("String:");
        for(int i=0; i< 10000; i++) {     //цикл в которий виполняется пока і < 10 000 (к надписи RadioRoc's 10 000 раз добавить s)
            String p = "s";
            s += p ;
            if (i == 0) {
                startTime = System.currentTimeMillis();
                System.out.println(" Start time - " + startTime);
            }
        }
        endTime = System.currentTimeMillis();
        System.out.println(" End time -   " + endTime);
        System.out.println(" Difference - " + (endTime - startTime));
        //System.out.println(" Text: " + s);
    }

    private static void testStringBuffer() {
        long startTime = 0, endTime;
        StringBuffer stringBuffer = new StringBuffer();
        System.out.println("StingBuffer:");
        for (int i = 0; i < 10000; i++) {  // к надписи  KISS FM добавляем 10 000 раз надпись KISS FM
            String p = "KISS FM ";
            p += p ;
            if (i == 0) {
                startTime = System.currentTimeMillis();
                System.out.println(" Start time - " + startTime);
            }

            stringBuffer.append(p);
        }
        endTime = System.currentTimeMillis();
        System.out.println(" End time -   " + endTime);
        System.out.println(" Difference - " + (endTime - startTime));
       // System.out.println(" Text: " + stringBuffer);
    }

    private static void testStringBuilder() {
        long startTime = 0, endTime;
        StringBuilder stringBuilder = new StringBuilder();
        System.out.println("StingBuilder:");
        for (int i = 0; i < 10000; i++) {   // к надписи  BBC news добавляем 10 000 раз BBC news
            String p = "BBC news ";
            p += p ;
            if (i == 0) {
                startTime = System.currentTimeMillis();
                System.out.println(" Start time - " + startTime);
            }
            stringBuilder.append(p);
        }
        endTime = System.currentTimeMillis();
        System.out.println(" End time -   " + endTime);
        System.out.println(" Difference - " + (endTime - startTime));
        //System.out.println(" Text: " + stringBuilder);
    }

}
