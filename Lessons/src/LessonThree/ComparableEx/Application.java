package LessonThree.ComparableEx;

/**
 * Created with IntelliJ IDEA.
 * User: User
 * Date: 02.11.13
 * Time: 13:30
 * To change this template use File | Settings | File Templates.
 */
public class Application {
    public static void main(String[] args) {
        Comparable[] sortedElements;

        Student[] students = new Student[3];
        students[0] = new Student(52645,"Smith","Bob");
        students[1] = new Student(98765,"Jones","Will");
        students[2] = new Student(1354,"Johnson","Matt");

        Airport[] airports = {
                new Airport("Киев","Международный аэропорт «Борисполь»"),
                new Airport("Симферополь","Международный аэропорт «Симферополь»"),
                new Airport("Донецк","Международный аэропорт «Донецк»"),
                new Airport("Одесса","Международный аэропорт «Одесса»"),
                new Airport("Киев","Киев-Жуляны"),
                new Airport("Львов","Международный аэропорт «Львов»"),
                new Airport("Харьков","Международный аэропорт «Харьков»"),
                new Airport("Днепропетровск","Международный аэропорт «Днепропетровск»"),
                new Airport("Запорожье","Международный аэропорт «Запорожье»"),
                new Airport("Ивано-Франковск","Международный аэропорт «Ивано-Франковск»")
        };


        sortedElements = Sorter.sort(students);
        System.out.println("Studens before sorting:");
        for  (Student student : students){
            student.print();
        }
        System.out.println();

        System.out.println("Studens after sorting");
        for (Student student : (Student[]) sortedElements){
            student.print();
        }
        System.out.println();

        sortedElements = Sorter.sort(airports);
        System.out.println("Airports before sorting:");
        for  (Airport airport : airports){
            airport.print();
        }
        System.out.println();

        System.out.println("Airports after sorting");
        for (Airport airport : (Airport[]) sortedElements){
            airport.print();
        }
    }

}
