package LessonThree.ComparableEx;

/**
 * Created with IntelliJ IDEA.
 * User: User
 * Date: 02.11.13
 * Time: 16:00
 * To change this template use File | Settings | File Templates.
 */
public class Student implements Comparable{

    public int student_id;
    public String last_name;
    public String first_name;

    Student(int student_id, String last_name, String first_name)
    {
        this.student_id = student_id;
        this.last_name = last_name;
        this.first_name = first_name;
    }

	  /* Перегрузка метода compareTo */

    public int compareTo(Object obj)
    {
        Student tmp = (Student)obj;
        if(this.student_id < tmp.student_id)
        {
	      /* текущее меньше полученного */
            return -1;
        }
        else if(this.student_id > tmp.student_id)
        {
	      /* текущее больше полученного */
            return 1;
        }
	    /* текущее равно полученному */
        return 0;
    }


    public void print() {
        System.out.printf("%s\t%s\t%d$\n",last_name,first_name,student_id);
    }
}