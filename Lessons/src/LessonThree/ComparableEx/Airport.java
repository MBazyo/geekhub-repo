package LessonThree.ComparableEx;

/**
 * Created with IntelliJ IDEA.
 * User: User
 * Date: 02.11.13
 * Time: 16:29
 * To change this template use File | Settings | File Templates.
 */
public class Airport implements Comparable<Airport> {
    private String nameAir;
    private String city;

    public Airport(String name, String city) {
        this.nameAir = name;
        this.city = city;
    }

    @Override
    public int compareTo(Airport o) {
        return this.city.compareTo(o.city);
    }

    public void print() {
        System.out.printf("%s %s\n",nameAir, city);
    }
}
