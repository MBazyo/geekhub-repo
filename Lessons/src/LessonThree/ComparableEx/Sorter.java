package LessonThree.ComparableEx;

/**
 * Created with IntelliJ IDEA.
 * User: User
 * Date: 02.11.13
 * Time: 13:15
 * To change this template use File | Settings | File Templates.
 */
public class Sorter {
    public static Comparable[] sort(Comparable[] elements) {
        Comparable[] array = elements.clone();
        int N = array.length;

        for (int i = 1; i < N; i++) {
            for (int j = i; (j > 0) && less(array[j], array[j - 1]); j--) {
                exch(array, j, j - 1);
            }
        }
        return array;
    }

    private static boolean less(Comparable v, Comparable w) {
        return v.compareTo(w) < 0;
    }

    private static void exch(Comparable[] a, int i, int j) {
        Comparable t = a[i];
        a[i] = a[j];
        a[j] = t;
    }
}
