package LessonThree.Car;
 import LessonThree.Car.LetsGo;
 import LessonTwo.Car;
 import LessonTwo.Driveable;

 import java.io.BufferedReader;
 import java.io.IOException;
 import java.io.InputStreamReader;

/**
 * Created with IntelliJ IDEA.
 * User: User
 * Date: 29.10.13
 * Time: 18:48
 * To change this template use File | Settings | File Templates.
 */
public class CarTwo extends Car  implements Driveable,LetsGo {
    double maxFuel = 60;
    double maxSpeed = 220;
    double fuelConsumption = 6;

    @Override
    public  void gasTank(){
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the amount of fuel in your gas-tank(size " +maxFuel+") in car:");

        try{
            fuel =Double.parseDouble(br.readLine());

            if(fuel>0&&fuel<60){
                System.out.print("We've got ");
                System.out.printf("%8.2f", fuel);
                System.out.println(" liters of fuel");

            } else if(fuel>60){
                System.out.println("Fuel does not fit in the tank");

            }else if(fuel==0) {
                System.out.println("Is no fuel in the gas - tank");
            }

        } catch (IOException e) {
            System.err.println("Error: You entered text");
        }

    }

    public  void accelerate(){
        this.engine(0.2,0.3);
    }

    public void turn(){
        // вивести надпись поворот + потреблять топливо привызове метода -0.01
        fuel = fuel-0.1;
        this.wheels();
        System.out.println("We are turn");

    }
    public void brake(){
        // остановить все процесы и вывести остаток топлива в баке
        moment = 0;
        speed = 0;
        System.out.println("The remaining fuel in the tank "+fuel);

    }

    public void goHundredMiles(){
        if(fuel>=6){
            fuel=fuel-fuelConsumption;
        }else if(fuel<6){
            System.out.println("Is no fuel in the gas - tank");
        }

    }

    public void tankUp10Liters(){
        if(maxFuel<=50){
            fuel=fuel+10;
        }else{
            System.out.println("Do you have enough fuel.");
        }
    }
    public void accelerateSpeed(){
        while(speed <= 210){
            speed = speed+10;
            fuel=fuel-0.2;
            break;
        }
    }
    public void lowerSpeed(){
        while(speed > 0 && speed < 220){
            speed = speed-10;
            fuel=fuel-0.2;
            break;
        }
    }
    public void showFuel(){
        System.out.print("We've got ");
        System.out.printf("%8.2f", fuel);
        System.out.println(" liters of fuel");
    }



}
