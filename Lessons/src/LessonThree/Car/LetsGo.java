package LessonThree.Car;

/**
 * Created with IntelliJ IDEA.
 * User: User
 * Date: 29.10.13
 * Time: 19:32
 * To change this template use File | Settings | File Templates.
 */
public interface LetsGo {
    void goHundredMiles();
    void tankUp10Liters();
    void accelerateSpeed();
    void lowerSpeed();
    void showFuel();

}
