package LessonTwo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

abstract class Vehicle {
    public static double fuel;
    public double moment; //скорость оборота колеса
    public double speed;

   abstract void gasTank();


    protected void engine(double coefFuel,double coefMoment){  //потребляе топливо = получаем момент
       if(fuel>0){
        fuel = fuel*coefFuel;
        moment= moment/coefMoment;
        speed = speed + moment;
       } else {
           System.out.println("Out of gas");
       }
        System.out.println("Our speed = "+speed);
    }

    protected void wheels(){  // получает момент = крутим колеса(текст)
        System.out.println("Wheels spinning at a rate of "+moment);
    }
}
