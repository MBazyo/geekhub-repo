package LessonTwo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created with IntelliJ IDEA.
 * User: User
 * Date: 22.10.13
 * Time: 20:02
 * To change this template use File | Settings | File Templates.
 */
public class SolarCar extends Vehicle implements Driveable{
    private boolean isRuning = false;
    @Override
    public  void gasTank(){

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("How much charge is left in your battery(in percentage)???");

        try{
            fuel =Double.parseDouble(br.readLine());
            if( fuel > 0){
                System.out.print("We've got ");
                System.out.printf("%8.2f", fuel);
                System.out.println(" charge");

            } else {
                System.out.println("There is no charge in the battery");
            }

        } catch (IOException e) {
            System.err.println("Error: You entered text");
        }

    }

    public  void accelerate(){

        this.engine(4,6);
    }

    public void turn(){

        fuel = fuel-1;
        this.wheels();
        System.out.println("We are turn");

    }
    public void brake(){

        moment = 0;
        speed = 0;
        System.out.println("The remaining fuel in the tank "+fuel);
    }

}
