package LessonTwo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Car extends Vehicle implements Driveable{

    private boolean isRuning = false;

    @Override
    public  void gasTank(){

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the amount of fuel in your gas-tank in car:");

        try{
            fuel =Double.parseDouble(br.readLine());
            if( fuel > 0){
                System.out.print("We've got ");
                System.out.printf("%8.2f", fuel);
                System.out.println(" liters of fuel");

            } else {
                System.out.println("Is no fuel in the gas - tank");
            }

        } catch (IOException e) {
            System.err.println("Error: You entered text");
        }

    }

    public  void accelerate(){
        //момент увиличивается = потребления топлива соответственно
        this.engine(0.2,0.3);
    }

    public void turn(){
        // вивести надпись поворот + потреблять топливо привызове метода -0.01
        fuel = fuel-0.01;
        this.wheels();
        System.out.println("We are turn");

    }
    public void brake(){
      // остановить все процесы и вывести остаток топлива в баке
       moment = 0;
       speed = 0;
       System.out.println("The remaining fuel in the tank "+fuel);
    }



}
