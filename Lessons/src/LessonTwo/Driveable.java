package LessonTwo;

public interface Driveable {
    void accelerate();
    void brake();
    void turn();


}
